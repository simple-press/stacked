<?php
# --------------------------------------------------------------------------------------
#
#	Simple:Press Theme custom function file
#	Theme		:	stacked
#	File		:	custom functions
#	Author		:	Simple:Press
#
#	The 'functions' file can be used for custom functions & is loaded with each template
#
# --------------------------------------------------------------------------------------

# A small javascript routine has been used to replace standard browser tooltips with
# more appealing graphics. You can turn this off by setting SP_TOOLTIPS to false.

if (!defined('SP_TOOLTIPS')) define('SP_TOOLTIPS', true);

# ------------------------------------------------------------------------------------------

#include a couple of files...
if (!defined('SPSTACKEDADMIN')) define('SPSTACKEDADMIN', SPTHEMEBASEDIR.'stacked/admin/');
require_once SPSTACKEDADMIN.'spAdmin.php';

# add version 2 theme flag
add_theme_support('level-2-theme');

# add support for child overlays
add_theme_support('sp-theme-child-overlays');

add_action('init', 'spStacked_textdomain');

# load the theme textdomain for translations
function spStacked_textdomain() {
	sp_theme_localisation('spStacked');
}
