<?php
/*
Simple:Press
Stacked Theme Admin Options Form
$LastChangedDate: 2019-04-25 07:30:12 +0100 (Thur, 25 April 2015) $
$Rev: 11958 $
*/

if (preg_match('#'.basename(__FILE__).'#', $_SERVER['PHP_SELF'])) die('Access denied - you cannot directly call this file');

function sp_stacked_options_form() {

	$st_opt_badgedirection = sp_stacked_options_get_badge_direction();

	$badge_values = sp_stacked_options_get_badge_options();

	spa_paint_open_tab(__('Stacked Theme Options', 'spStacked'));
	
		spa_paint_open_panel();
    		spa_paint_open_fieldset(__('Special Rank Badge Options', 'spStacked'), true, 'stacked-theme-badge-options');				
				spa_paint_radiogroup(__('What direction would like to display your badges?', 'spStacked'), 'sfstacked-badge-option', $badge_values, $st_opt_badgedirection, false, true);
    		spa_paint_close_fieldset();
		spa_paint_close_panel();

		spa_paint_tab_right_cell();

		//spa_paint_close_container();

	spa_paint_close_tab();	

}

?>