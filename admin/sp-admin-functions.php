<?php
/*
Simple:Press
Functions used across various admin areas for the stacked theme
$LastChangedDate: 2019-04-25 07:30:12 +0100 (Thur, 25 April 2015) $
$Rev: 11958 $
*/

if (preg_match('#'.basename(__FILE__).'#', $_SERVER['PHP_SELF'])) die('Access denied - you cannot directly call this file');

/**
 * Return a list of acceptable options for displaying badges
 *
 * @access public
 *
 * @since 2.1
 *
 * @param none
 *
 * @returns array $values
 */
function sp_stacked_options_get_badge_options() {
	$values = array(SP()->primitives->admin_text('Vertical'),
					SP()->primitives->admin_text('Horizontal'));
								
	return $values;
}

/**
 * Return a specific admin option from the database that is 
 * related to the stacked theme.
 *
 * @access public
 *
 * @since 2.1
 *
 * @param none
 *
 * @returns int 
 */
function sp_stacked_options_get_badge_direction() {

	// Get the existing option values from the database
 	$stackedoptions = SP()->options->get('stackedtheme');
	
	// Get badge option from the options array.  Set a default if none is available.
	$st_opt_badgedirection = 1;
	if ( isset( $stackedoptions['special-rank-badge-direction'] ) ) {
		$st_opt_badgedirection = (int) $stackedoptions['special-rank-badge-direction'];  // 1 = stacked vertical 2 = stacked horizontal, default 1.  No other value allowed hence the casting to an "int" for sanitization.
	}
	if (empty($st_opt_badgedirection)) {
		$st_opt_badgedirection = 1;
	}
	
	return $st_opt_badgedirection;
	
}

/**
 * Save a specific admin option to the database that is 
 * related to the stacked theme.
 *
 * @access public
 *
 * @since 2.1
 *
 * @param string $option_name
 * @param string $option_value
 *
 * @returns boolean 
 */
function sp_save_stacked_options($option_name,$option_value) {

	// Get the existing option values from the database
 	$stackedoptions = SP()->options->get('stackedtheme');
	
	if ( 'special-rank-badge-direction' == $option_name ) {
		// Sanitize option value and save to options array
		$stackedoptions[$option_name] = (int) $option_value;	
	}
	
	// Save to database and return...
 	return SP()->options->update('stackedtheme', $stackedoptions);	
	
}

?>