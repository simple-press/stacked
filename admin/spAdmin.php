<?php
/*
Simple:Press
Stacked SP Theme Admin Custom Control
$LastChangedDate: 2019-04-25 07:30:12 +0100 (Thur, 25 April 2015) $
$Rev: 11958 $
*/

if (preg_match('#'.basename(__FILE__).'#', $_SERVER['PHP_SELF'])) die('Access denied - you cannot directly call this file');

if (!defined('SPSTACKEDADMIN')) define('SPSTACKEDADMIN', SPTHEMEBASEDIR.'stacked/admin/');

add_action('sph_admin_menu',				'sp_stacked_custom_menu');
add_filter('init', 							'sp_stacked_init');  
add_filter('sph_admin_help-admin-themes',	'sp_stacked_custom_help', 10, 3);


function sp_stacked_init() {
	require_once SPSTACKEDADMIN.'sp-admin-functions.php';
}

function sp_stacked_custom_menu() {
	$subpanels = array(
		__('Stacked Custom Options', 'spStacked') => array('admin' => 'sp_stacked_options', 'save' => 'sp_stacked_options_save', 'form' => 1, 'id' => 'stacked')
	);
	SP()->plugin->add_admin_subpanel('themes', $subpanels);
}

function sp_stacked_options() {
    require_once SPSTACKEDADMIN.'sp-stacked-options-form.php';
	sp_stacked_options_form();
}

function sp_stacked_options_save() {
    require_once SPSTACKEDADMIN.'sp-stacked-options-save.php';
	sp_stacked_options_save_all();
}

function sp_stacked_custom_help($file, $tag, $lang) {
    if ($tag == '[stacked-theme-badge-options]') $file = SPSTACKEDADMIN.'sp-stacked-options-help.'.$lang;
    return $file;
}
