<?php
/*
Simple:Press
Stacked Theme Admin Options Save Routine
$LastChangedDate: 2019-04-25 07:30:12 +0100 (Thur, 25 April 2015) $
$Rev: 11958 $
*/

if (preg_match('#'.basename(__FILE__).'#', $_SERVER['PHP_SELF'])) die('Access denied - you cannot directly call this file');

function sp_stacked_options_save_all() {
	
	// @Todo: Is the admin referrer correct below?
	check_admin_referer('forum-adminform_userplugin', 'forum-adminform_userplugin');

	// Get values from user input and sanitize by forcing to an int.  Simple:Press doesn't have a SP()->saveFilters->int function
	$i_badge_direction = (int) trim($_POST['sfstacked-badge-option']);

	// Save to database
	sp_save_stacked_options('special-rank-badge-direction',$i_badge_direction);
	
	// Show message on screen.
	echo __('Stacked Theme Options Updated', 'spStacked');
	
	
}

?>