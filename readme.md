# README #

This project is an extension designed to run on the Simple:Press Wordpress Theme Platform.  

### How do I get set up? ###

This theme is not a normal WordPress theme and cannot be installed via the WordPress THEMES screen.
Instead, you should:

- Download the theme file from your receipt or from your dashboard
- Go to FORUM->Themes
- click on `Theme Uploader`
- Click on the `Choose File` button and select the zip file you downloaded earlier
- Click the `Upload Now` button
- Go back to the FORUM->Themes screen and activate the theme.


### Change Log  ###
-----------------------------------------------------------------------------------------
###### 2.1.2
- Tweak: New preview image

###### 2.1.1
- Fix: The new post view was broken because of a 5.0 style function call.

###### 2.1.0
- New: Option to set special rank badges to display horizontally or vertically.

###### 2.0.0
- New: 6.0 compatibility

###### 1.0.0
- Initial release

### Known Issues  ###
-----------------------------------------------------------------------------------------
- [B0002] Admin referrer in file sp-stacked-options-save needs to be double-checked to see if its used correctly.
